using System;

namespace MusicLibrarySuite.Infrastructure.Microservices.Attributes;

/// <summary>
/// Specifies a microservice name for the assembly.
/// </summary>
[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
public sealed class AssemblyMicroserviceNameAttribute : Attribute
{
    /// <summary>
    /// Gets the Kubernetes microservice name.
    /// </summary>
    /// <remarks>
    /// The hyphen-separated lowercase Kubernetes resource name, e.g. <c>example-service</c>.
    /// </remarks>
    public string MicroserviceName { get; }

    /// <summary>
    /// Gets the .NET microservice name.
    /// </summary>
    /// <remarks>
    /// The period-separated Pascal case .NET root namespace name, e.g. <c>MusicLibrarySuite.ExampleService</c>.
    /// </remarks>
    public string MicroserviceNamePascalCase { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="AssemblyMicroserviceNameAttribute" /> type using the specified values.
    /// </summary>
    /// <param name="microserviceName">The Kubernetes microservice name.</param>
    /// <param name="microserviceNamePascalCase">The .NET microservice name.</param>
    public AssemblyMicroserviceNameAttribute(string microserviceName, string microserviceNamePascalCase)
    {
        MicroserviceName = microserviceName;
        MicroserviceNamePascalCase = microserviceNamePascalCase;
    }
}
