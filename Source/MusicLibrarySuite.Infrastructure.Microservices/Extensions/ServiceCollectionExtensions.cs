using System;
using System.Reflection;

using Microsoft.Extensions.DependencyInjection;

using MusicLibrarySuite.Infrastructure.Microservices.Services;
using MusicLibrarySuite.Infrastructure.Microservices.Services.Abstractions;

namespace MusicLibrarySuite.Infrastructure.Microservices.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IServiceCollection" /> interface.
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Registers the <see cref="IMicroserviceInfoProvider" /> service in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection to add services to.</param>
    /// <param name="currentMicroserviceAssembly">The current microservice assembly containing the Startup.cs file.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddMicroserviceInfoProvider(this IServiceCollection services, Assembly currentMicroserviceAssembly)
    {
        return services.AddSingleton<IMicroserviceInfoProvider, MicroserviceInfoProvider>(serviceProvider =>
        {
            return new MicroserviceInfoProvider(currentMicroserviceAssembly);
        });
    }

    /// <summary>
    /// Registers the <see cref="IMicroserviceInfoProvider" /> service in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection to add services to.</param>
    /// <param name="startupType">The Startup type to get the current microservice assembly from.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddMicroserviceInfoProvider(this IServiceCollection services, Type startupType)
    {
        return services.AddMicroserviceInfoProvider(startupType.Assembly);
    }

    /// <summary>
    /// Registers the <see cref="IMicroserviceInfoProvider" /> service in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection to add services to.</param>
    /// <param name="startupObject">The Startup object to get the current microservice assembly from.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddMicroserviceInfoProvider(this IServiceCollection services, object startupObject)
    {
        return services.AddMicroserviceInfoProvider(startupObject.GetType());
    }

    /// <summary>
    /// Registers the <see cref="IMicroserviceInfoProvider" /> service in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <typeparam name ="TStartup">The Startup type to get the current microservice assembly from.</typeparam>
    /// <param name="services">The <see cref="IServiceCollection" /> collection to add services to.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddMicroserviceInfoProvider<TStartup>(this IServiceCollection services)
        where TStartup : class
    {
        return services.AddMicroserviceInfoProvider(typeof(TStartup));
    }
}
