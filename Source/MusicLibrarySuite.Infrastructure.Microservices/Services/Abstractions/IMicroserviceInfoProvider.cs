using System;
using System.Diagnostics.CodeAnalysis;

namespace MusicLibrarySuite.Infrastructure.Microservices.Services.Abstractions;

/// <summary>
/// Describes a microservice information provider.
/// </summary>
public interface IMicroserviceInfoProvider
{
    /// <summary>
    /// Gets an object that describes the current microservice.
    /// </summary>
    /// <returns>The <see cref="MicroserviceInfo" /> object that describes the current microservice.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the microservice assembly is not marked with the required attributes.</exception>
    public MicroserviceInfo GetCurrentMicroserviceInfo();

    /// <summary>
    /// Gets an object that describes an external microservice by an external interface type.
    /// </summary>
    /// <param name="externalMicroserviceType">The external interface type from the assembly representing the external microservice.</param>
    /// <returns>The <see cref="MicroserviceInfo" /> object that describes the external microservice.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the microservice assembly is not marked with the required attributes.</exception>
    public MicroserviceInfo GetExternalMicroserviceInfo(Type externalMicroserviceType);

    /// <summary>
    /// Gets an object that describes an external microservice by an external interface object.
    /// </summary>
    /// <param name="externalMicroserviceObject">The external interface object of type from the assembly representing the external microservice.</param>
    /// <returns>The <see cref="MicroserviceInfo" /> object that describes the external microservice.</returns>
    /// <exception cref="InvalidOperationException">Thrown if the microservice assembly is not marked with the required attributes.</exception>
    public MicroserviceInfo GetExternalMicroserviceInfo(object externalMicroserviceObject);

    /// <summary>
    /// Gets an object that describes the current microservice.
    /// </summary>
    /// <param name="microserviceInfo">When this method returns, contains the <see cref="MicroserviceInfo" /> object that describes the current microservice, if it is available; otherwise, <see langword="null" />. This parameter is passed uninitialized.</param>
    /// <returns><see langword="true" /> if the microservice assembly is marked with the required attributes; otherwise, <see langword="false" />.</returns>
    public bool TryGetCurrentMicroserviceInfo([MaybeNullWhen(false)] out MicroserviceInfo microserviceInfo);

    /// <summary>
    /// Gets an object that describes an external microservice by an external interface type.
    /// </summary>
    /// <param name="externalMicroserviceType">The external interface type from the assembly representing the external microservice.</param>
    /// <param name="microserviceInfo">When this method returns, contains the <see cref="MicroserviceInfo" /> object that describes the external microservice, if it is available; otherwise, <see langword="null" />. This parameter is passed uninitialized.</param>
    /// <returns><see langword="true" /> if the microservice assembly is marked with the required attributes; otherwise, <see langword="false" />.</returns>
    public bool TryGetExternalMicroserviceInfo(Type externalMicroserviceType, [MaybeNullWhen(false)] out MicroserviceInfo microserviceInfo);

    /// <summary>
    /// Gets an object that describes an external microservice by an external interface object.
    /// </summary>
    /// <param name="externalMicroserviceObject">The external interface object of type from the assembly representing the external microservice.</param>
    /// <param name="microserviceInfo">When this method returns, contains the <see cref="MicroserviceInfo" /> object that describes the external microservice, if it is available; otherwise, <see langword="null" />. This parameter is passed uninitialized.</param>
    /// <returns><see langword="true" /> if the microservice assembly is marked with the required attributes; otherwise, <see langword="false" />.</returns>
    public bool TryGetExternalMicroserviceInfo(object externalMicroserviceObject, [MaybeNullWhen(false)] out MicroserviceInfo microserviceInfo);
}
