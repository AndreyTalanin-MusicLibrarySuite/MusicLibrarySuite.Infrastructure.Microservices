using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

using MusicLibrarySuite.Infrastructure.Microservices.Attributes;
using MusicLibrarySuite.Infrastructure.Microservices.Services.Abstractions;

namespace MusicLibrarySuite.Infrastructure.Microservices.Services;

/// <summary>
/// Represents a microservice information provider.
/// </summary>
internal class MicroserviceInfoProvider : IMicroserviceInfoProvider
{
    private readonly Assembly m_currentMicroserviceAssembly;

    /// <summary>
    /// Initializes a new instance of the <see cref="MicroserviceInfoProvider" /> type using the specified current microservice assembly.
    /// </summary>
    /// <param name="currentMicroserviceAssembly">The current microservice assembly containing the Startup.cs file.</param>
    public MicroserviceInfoProvider(Assembly currentMicroserviceAssembly)
    {
        m_currentMicroserviceAssembly = currentMicroserviceAssembly;
    }

    /// <inheritdoc />
    public MicroserviceInfo GetCurrentMicroserviceInfo()
    {
        return !TryGetCurrentMicroserviceInfo(out MicroserviceInfo? microserviceInfo)
            ? throw new InvalidOperationException($"Unable to get the microservice description from the {m_currentMicroserviceAssembly.FullName} assembly.")
            : microserviceInfo;
    }

    /// <inheritdoc />
    public MicroserviceInfo GetExternalMicroserviceInfo(Type externalMicroserviceType)
    {
        Assembly externalMicroserviceAssembly = externalMicroserviceType.Assembly;
        return !TryGetExternalMicroserviceInfo(externalMicroserviceType, out MicroserviceInfo? microserviceInfo)
            ? throw new InvalidOperationException($"Unable to get the microservice description from the {externalMicroserviceAssembly.FullName} assembly.")
            : microserviceInfo;
    }

    /// <inheritdoc />
    public MicroserviceInfo GetExternalMicroserviceInfo(object externalMicroserviceObject)
    {
        return GetExternalMicroserviceInfo(externalMicroserviceObject.GetType());
    }

    /// <inheritdoc />
    public bool TryGetCurrentMicroserviceInfo([MaybeNullWhen(false)] out MicroserviceInfo microserviceInfo)
    {
        return TryGetMicroserviceInfo(m_currentMicroserviceAssembly, out microserviceInfo);
    }

    /// <inheritdoc />
    public bool TryGetExternalMicroserviceInfo(Type externalMicroserviceType, [MaybeNullWhen(false)] out MicroserviceInfo microserviceInfo)
    {
        Assembly externalMicroserviceAssembly = externalMicroserviceType.Assembly;
        return TryGetMicroserviceInfo(externalMicroserviceAssembly, out microserviceInfo);
    }

    public bool TryGetExternalMicroserviceInfo(object externalMicroserviceObject, [MaybeNullWhen(false)] out MicroserviceInfo microserviceInfo)
    {
        return TryGetExternalMicroserviceInfo(externalMicroserviceObject.GetType(), out microserviceInfo);
    }

    private static bool TryGetMicroserviceInfo(Assembly assembly, [MaybeNullWhen(false)] out MicroserviceInfo microserviceInfo)
    {
        AssemblyMicroserviceNameAttribute? assemblyMicroserviceNameAttribute = assembly.GetCustomAttribute<AssemblyMicroserviceNameAttribute>();
        if (assemblyMicroserviceNameAttribute is null)
        {
            microserviceInfo = null;
            return false;
        }

        microserviceInfo = new MicroserviceInfo(
            assemblyMicroserviceNameAttribute.MicroserviceName,
            assemblyMicroserviceNameAttribute.MicroserviceNamePascalCase);
        return true;
    }
}
